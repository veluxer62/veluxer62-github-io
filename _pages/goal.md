---
permalink: /goal/
title: "목표"
---

## 2020년 목표

### 연간 목표

- [ ] Graph SQL/DB 마스터하기
- [ ] 예약봇 개선하기
- [ ] 책 10권 이상 읽기
- [ ] 블로그 40개 이상 작성하기
- [ ] 아이엘츠 시험 치기
- [ ] 71kg 몸무게 만들기

### 월간 목표

<details><summary>책 1권 이상 읽기</summary>
<pre>
<progress value="1" max="12"></progress> 1/12 (<b>8%</b>)
</pre>
</details>

### 주간 목표

<details><summary>블로그 1개 이상 작성하기</summary>
<pre>
 1월 <progress value="2" max="4"></progress> 2/4 (<b>50%</b>)
 2월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 3월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 4월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 5월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 6월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 7월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 8월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 9월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
10월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
11월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
12월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
</pre>
</details>

<details><summary>주간 회고 작성하기</summary>
<pre>
 1월 <progress value="3" max="4"></progress> 3/4 (<b>75%</b>)
 2월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 3월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 4월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 5월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 6월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 7월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
 8월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
 9월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
10월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
11월 <progress value="0" max="5"></progress> 0/5 (<b>0%</b>)
12월 <progress value="0" max="4"></progress> 0/4 (<b>0%</b>)
</pre>
</details>

### 일일 목표

<details><summary>영어공부 30분 이상 하기</summary>
<pre>
 1월 <progress value="20" max="31"></progress> 20/31 (<b>65%</b>)
 2월 <progress value="0" max="29"></progress> 0/29 (<b>0%</b>)
 3월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 4월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
 5월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 6월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
 7월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 8월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 9월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
10월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
11월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
12월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
</pre>
</details>

<details><summary>운동 하기</summary>
<pre>
 1월 <progress value="13" max="31"></progress> 13/31 (<b>42%</b>)
 2월 <progress value="0" max="29"></progress> 0/29 (<b>0%</b>)
 3월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 4월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
 5월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 6월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
 7월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 8월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
 9월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
10월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
11월 <progress value="0" max="30"></progress> 0/30 (<b>0%</b>)
12월 <progress value="0" max="31"></progress> 0/31 (<b>0%</b>)
</pre>
</details>
